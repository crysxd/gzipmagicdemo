package com.example.myapplication

import io.ktor.client.HttpClient
import io.ktor.client.plugins.cache.HttpCache
import io.ktor.client.plugins.compression.ContentEncoding
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.plugins.onDownload
import io.ktor.client.request.get
import io.ktor.client.request.headers
import io.ktor.client.statement.bodyAsText
import io.ktor.http.CacheControl
import io.ktor.http.ContentType
import io.ktor.http.Headers
import io.ktor.http.HttpHeaders
import io.ktor.http.content.CachingOptions
import io.ktor.http.content.OutgoingContent
import io.ktor.server.application.call
import io.ktor.server.application.install
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.netty.NettyApplicationEngine
import io.ktor.server.plugins.cachingheaders.CachingHeaders
import io.ktor.server.plugins.compression.Compression
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import io.netty.handler.codec.compression.StandardCompressionOptions.gzip
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class GzipMagicTest {

    private lateinit var server: NettyApplicationEngine
    private val lastModified = Date(System.currentTimeMillis() - 10000)

    @Before
    fun startServer() {
        server = embeddedServer(Netty, port = 4444) {
            install(Compression) {
                gzip()
            }
            install(CachingHeaders) {
                options { _, _ ->
                    CachingOptions(
                        CacheControl.MaxAge(
                            86400,
                            visibility = CacheControl.Visibility.Public,
                            mustRevalidate = false
                        )
                    )
                }
            }
            routing {
                get("/gzip") {
                    val format = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz")
                    val bytes = downloadContent.toByteArray()

                    call.respond(object : OutgoingContent.ByteArrayContent() {
                        override fun bytes() = bytes

                        override val contentLength: Long
                            get() = bytes.size.toLong()

                        override val contentType: ContentType
                            get() = ContentType.Text.Plain

                        override val headers: Headers
                            get() = Headers.build {
                                append(HttpHeaders.ETag, "123")
                                append(HttpHeaders.LastModified, format.format(lastModified))
                            }

                    })
                }
            }
        }

        server.start(wait = false)
    }

    @After
    fun stopServer() = runBlocking {
        server.stop(gracePeriodMillis = 100)
        delay(500)
    }


    private fun doTest(
        useGzip: Boolean = true,
        useCache: Boolean = true,
        useInfoLogging: Boolean = true,
        useOnDownload: Boolean = true,
    ) = runBlocking {
        val client = HttpClient {
            if (useGzip) {
                install(ContentEncoding) {
                    gzip(0.9f)
                    // Using Deflate causes a invalid block type exception
                    // deflate(1.0f)
                }
            }

            if (useCache) {
                install(HttpCache)
            }

            install(Logging) {
                level = if (useInfoLogging) LogLevel.INFO else LogLevel.ALL
                logger = object : Logger {
                    override fun log(message: String) {
                        println(message)
                    }
                }
            }
        }

        val response = client.get(
            // Produces GZIP magic exception, not sure why because it's the same file
            // urlString = "http://100.64.161.19/downloads/files/local/CE3PRO_pikachu_1gen_flowalistik.gcode"
            // Produces max length execption
            urlString = "http://localhost:4444/gzip"
        ) {
            headers {
                append("X-Api-Key", "37C2E1504909444ABA2BE889E66A96AA")
            }

            if (useOnDownload) {
                onDownload { bytesSentTotal, contentLength ->
                    println("Progress: $bytesSentTotal $contentLength")
                }
            }
        }

        response.bodyAsText()

        assertEquals(
            expected = 200,
            actual = response.status.value
        )
    }

    @Test
    fun WHEN_using_all_THEN_magic_fails() {
        assertFailsWith<java.lang.IllegalArgumentException>(message = "max shouldn't be negative: -24798") {
            doTest()
        }
    }

    @Test
    fun WHEN_not_using_gzip_THEN_magic_succeeds() = doTest(
        useGzip = false,
    )

    @Test
    fun WHEN_not_using_cache_THEN_magic_succeeds() = doTest(
        useCache = false,
    )

    @Test
    fun WHEN_not_using_logging_THEN_magic_succeeds() = doTest(
        useInfoLogging = false,
    )

    @Test
    fun WHEN_not_using_dowload_THEN_magic_succeeds() = doTest(
        useOnDownload = false,
    )
}